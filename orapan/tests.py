from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
from orapan.models import Item
from orapan.views import home_page
import datetime
import pep8
import unittest


class TestCodeFormat(unittest.TestCase):
    def test_pep8_unittest(self):
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('orapan/tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("\nunittest found %s errors (and warnings)" % file_errors)

    def test_pep8_view(self):
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('orapan/views.py', show_source=True)
        file_errors = fchecker.check_all()
        print("\nview found %s errors (and warnings)" % file_errors)

    def test_pep8_model(self):
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('orapan/models.py', show_source=True)
        file_errors = fchecker.check_all()
        print("\nmodel found %s errors (and warnings)" % file_errors)

    def test_pep8_functionaltest(self):
        """Test that we conform to PEP8."""
        fchecker = pep8.Checker('functional_tests.py', show_source=True)
        file_errors = fchecker.check_all()
        print("\nfunctionaltest found %s errors (and warnings)" % file_errors)


class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_to_show_list_items(self):
        Item.objects.create(money='20')
        Item.objects.create(money='50')
        request = HttpRequest()
        response = home_page(request)
        Day = datetime.date.today().strftime('%Y-%m-%d')
        self.assertIn('20', response.content.decode())  # first input
        self.assertIn(Day, response.content.decode())
        self.assertIn('50', response.content.decode())  # second input
        self.assertIn('35', response.content.decode())  # average
        self.assertIn('70', response.content.decode())  # total
        # #min2max
        self.assertIn('1 : 20', response.content.decode())
        self.assertIn('2 : 50', response.content.decode())
        # #max2min
        self.assertIn('1 : 50', response.content.decode())
        self.assertIn('2 : 20', response.content.decode())

    def test_save_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['money_text'] = '20'
        response = home_page(request)
        # #check data have 1 item after save
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        Day = datetime.date.today().strftime('%Y-%m-%d')
        self.assertEqual(new_item.money, 20.0)
        self.assertEqual(new_item.date, Day)

    def test_Redirects(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['money_text'] = '50'
        response = home_page(request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Item.objects.count(), 0)


class ItemModelTest(TestCase):
    def test_saving_and_retrieving_items(self):
        first_item = Item()
        first_item.money = '50'
        first_item.save()

        second_item = Item()
        second_item.money = '150'
        second_item.save()

        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        Day = datetime.date.today().strftime('%Y-%m-%d')
        self.assertEqual(first_saved_item.money, 50.0)
        self.assertEqual(first_saved_item.date, Day)
        self.assertEqual(second_saved_item.money, 150.0)
        self.assertEqual(second_saved_item.date, Day)
