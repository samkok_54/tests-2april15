from django.shortcuts import redirect, render
from orapan.models import Item
import datetime


def home_page(request):
    if request.method == 'POST':
        Day = datetime.date.today().strftime('%Y-%m-%d')
        if request.POST.get('money_text', '') == '':
            moneyy = 0.0
        else:
            moneyy = float(request.POST.get('money_text', ''))
        Item.objects.create(date=Day, money=moneyy)
        return redirect('/')
    items = Item.objects.all()
    total = 0.0
    Average = 0.0
    Max = 0
    Min = 0
    datamoney = []
    maxTomin = []
    minTomax = []
    # #Total
    for item in items:
        total = total + float(item.money)
        datamoney.append(item.money)
    # #Average
    if Item.objects.count() != 0:
        Average = total / Item.objects.count()
        Max = max(datamoney)
        Min = min(datamoney)
        for j in range(len(datamoney)):
            F = datamoney[j]
            minTomax.append(F)
            maxTomin.append(F)
        minTomax.sort()
        maxTomin.sort(reverse=True)
    return render(request, 'home.html', {'items': items,
                                         'Total': total,
                                         'Average': Average,
                                         'Max': Max,
                                         'Min': Min,
                                         'minTomax': minTomax,
                                         'maxTomin': maxTomin})
