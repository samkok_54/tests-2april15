# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orapan', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='text',
        ),
        migrations.AddField(
            model_name='item',
            name='date',
            field=models.TextField(default='2015-04-02'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='money',
            field=models.TextField(default='0.0'),
            preserve_default=True,
        ),
    ]
