# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orapan', '0002_auto_20150402_0625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='date',
            field=models.TextField(default='2015-04-05'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='money',
            field=models.FloatField(default='0.0'),
            preserve_default=True,
        ),
    ]
