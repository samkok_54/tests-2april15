from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
import datetime


class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row(self, row_text, ids):
        table = self.browser.find_element_by_id(ids)
        rows = table.find_elements_by_tag_name('td')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Edith has heard about a cool new online to-do app. She goes
        # to check out its homepage
        self.browser.get('http://localhost:8000')
        # She notices the page title and header mention to-do lists
        self.assertIn('My test', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('My money', header_text)
        # check start input
        inputbox = self.browser.find_element_by_id('money_new_item')
        self.assertEqual(inputbox.get_attribute('placeholder'),
                         '00.00')
        # input valua
        inputbox.send_keys('40')
        # send data
        self.browser.find_element_by_id('submit_data').click()
        # check data that input , date , total , average
        self.check_for_row('40.0', 'id_list_table')
        self.check_for_row('40.0', 'TableOfTotal')
        self.check_for_row('40.0', 'TableOfA')
        self.check_for_row('40.0', 'TableOfMax')
        self.check_for_row('40.0', 'TableOfMin')
        # ###### second data
        # check start input
        inputbox = self.browser.find_element_by_id('money_new_item')
        self.assertEqual(inputbox.get_attribute('placeholder'),
                         '00.00')
        # input valua
        inputbox.send_keys('80')
        # send data
        self.browser.find_element_by_id('submit_data').click()
        # check data that input , date , total , average
        self.check_for_row('80.0', 'id_list_table')
        self.check_for_row('120.0', 'TableOfTotal')
        self.check_for_row('60.0', 'TableOfA')
        self.check_for_row('80.0', 'TableOfMax')
        self.check_for_row('40.0', 'TableOfMin')
        # check data sort
        self.browser.find_element_by_name('maxTomin').click()
        self.check_for_row('1 : 80.0', 'max-->min')
        self.check_for_row('2 : 40.0', 'max-->min')
        self.browser.find_element_by_name('minTomax').click()
        self.check_for_row('1 : 40.0', 'min-->max')
        self.check_for_row('2 : 80.0', 'min-->max')
        self.fail('Finish the test!')

if __name__ == '__main__':
    unittest.main(warnings='ignore')
